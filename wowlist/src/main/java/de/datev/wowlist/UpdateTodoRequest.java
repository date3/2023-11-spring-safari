package de.datev.wowlist;

import java.time.Instant;

public class UpdateTodoRequest {

    private String description;
    private boolean isDone;
    private Instant dueDate;

    public UpdateTodoRequest() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }
}
