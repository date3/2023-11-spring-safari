package de.datev.wowlist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class ExampleController {

    @Value("${app.name}")
    private String appName;

    @Autowired
    private WowlistConfigProperties config;

    Logger logger = LoggerFactory.getLogger(ExampleController.class);

    @GetMapping("/example")
    public String example() {
        System.out.println("Hello world was called!");
        this.logger.info("Hello world was logged");
        this.logger.error("Hello world error");
        return "Hello Matthias! From " + this.appName;
    }

    @GetMapping("/example/{id}")
    public String exampleWithId(@PathVariable String id){
        this.logger.info("Got id value {}", id);
        return "Id value from path ist: " + id;
    }

    @PostMapping("/echo")
    public String echo(@RequestBody String input){
        this.logger.debug("Received text: {}.", input);
        return input;
    }

    @PostMapping("/calculate/add")
    public ResponseEntity<Void> add(@RequestBody String calculation) {
        try {
            int firstSummand = Integer.parseInt(calculation.split("=")[0].split("\\+")[0]);
            int secondSummand = Integer.parseInt(calculation.split("=")[0].split("\\+")[1]);
            int result = Integer.parseInt(calculation.split("=")[1]);
            if (firstSummand + secondSummand == result) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/config")
    public String config() {
        return this.config.getConfig();
    }
}
