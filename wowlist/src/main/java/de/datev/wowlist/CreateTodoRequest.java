package de.datev.wowlist;

import java.time.Instant;

public class CreateTodoRequest {

    private String description;
    private Instant dueDate;

    public CreateTodoRequest() {
    }

    public CreateTodoRequest(String description, Instant dueDate) {
        this.description = description;
        this.dueDate = dueDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }
}
