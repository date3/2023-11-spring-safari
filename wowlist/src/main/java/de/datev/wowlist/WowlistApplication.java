package de.datev.wowlist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

@SpringBootApplication
public class WowlistApplication {

	public static void main(String[] args) {
		SpringApplication.run(WowlistApplication.class, args);
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http
				.csrf(csrf -> csrf.disable())
				.authorizeHttpRequests(authz -> {
					authz
							.requestMatchers(antMatcher("/actuator/**")).permitAll()
							.requestMatchers(antMatcher("/h2-console/**")).permitAll()
							.requestMatchers(antMatcher("/hello")).permitAll()
							.requestMatchers(antMatcher("/todos/**")).hasRole("ADMIN")
							.anyRequest().authenticated();
				}).httpBasic(Customizer.withDefaults());

		return http.build();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		User.UserBuilder builder = User.withDefaultPasswordEncoder();

		var user = builder
				.username("user")
				.password("password")
				.roles("USER")
				.build();

		UserDetails dagobert = builder
				.username("dagobert")
				.password("duck")
				.roles("ADMIN")
				.build();

		return new InMemoryUserDetailsManager(user, dagobert);
	}

}
