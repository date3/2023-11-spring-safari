package de.datev.wowlist;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

@Component
@Endpoint(id = "todos")
public class TodoActuator {

    private final TodosRepository repository;

    public TodoActuator(TodosRepository repository) {
        this.repository = repository;
    }

    @ReadOperation
    public TodoActuatorResult todoActuator() {
        var todoCount = repository.count();

        return new TodoActuatorResult(todoCount);
    }
}