package de.datev.wowlist;

public class AddSubtaskRequest {

    private String description;

    public AddSubtaskRequest() {
    }

    public AddSubtaskRequest(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
