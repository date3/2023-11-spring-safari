package de.datev.wowlist;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class TodosService {

    private final TodosRepository todosRepository;

    public TodosService(TodosRepository todosRepository) {
        this.todosRepository = todosRepository;
    }

    public List<Todo> getAllTodos() {
        return this.todosRepository.findAll();
    }

    public Todo createTodo(String description, Instant dueDate) {
        var newTodo = new Todo(description, dueDate);
        return this.todosRepository.save(newTodo);
    }

    public Todo getById(UUID id) {
        return this.todosRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public void deleteAll() {
        this.todosRepository.deleteAll();
    }

    public Todo update(UUID id, String description, boolean done, Instant dueDate) {
        var todo = this.getById(id);
        todo.setDescription(description);
        todo.setDone(done);
        todo.setDueDate(dueDate);
        return this.todosRepository.save(todo);
    }

    public void delete(UUID id) {
        var todo = this.getById(id);
        this.todosRepository.delete(todo);
    }

    public List<Todo> findByDescription(String description) {
        return this.todosRepository.findAllByDescription(description);
    }

    public List<Todo> findByDescriptionAndDueDate(String description, Instant dueDate) {
        return this.todosRepository.findAllByDescriptionAndDueDateBefore(description, dueDate);
    }

    public List<Todo> findByDueDateBefore(Instant dueDate) {
        return this.todosRepository.findAllByDueDateBefore(dueDate);
    }

    public void addSubTask(UUID id, String description) {
        var todo = this.getById(id);
        SubTask subTask = new SubTask(description);
        todo.addSubTask(subTask);
        this.todosRepository.save(todo);
    }
}
