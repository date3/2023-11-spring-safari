package de.datev.wowlist;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class TodoDTO {
    private UUID id;
    private String description;
    private boolean isDone;
    private Instant dueDate;

private List<SubTaskDTO> subTasks;

    public TodoDTO(UUID id, String description, boolean isDone, Instant dueDate, List<SubTask> subTasks) {
        this.id = id;
        this.description = description;
        this.isDone = isDone;
        this.dueDate = dueDate;
        this.subTasks = subTasks.stream()
                .map(subTask -> new SubTaskDTO(subTask.getId(), subTask.getDescription()))
                .collect(Collectors.toList());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }

    public List<SubTaskDTO> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(List<SubTaskDTO> subTasks) {
        this.subTasks = subTasks;
    }
}
