package de.datev.wowlist;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

import java.util.UUID;

@Entity
public class SubTask {

    @Id
    private UUID id;
    @Column
    private String description;

    @ManyToOne
    private Todo todo;

    protected SubTask() {
    }

    public SubTask(String description) {
        this.id = UUID.randomUUID();
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
        if (!todo.getSubTasks().contains(this)) {
            todo.addSubTask(this);
        }
    }
}
