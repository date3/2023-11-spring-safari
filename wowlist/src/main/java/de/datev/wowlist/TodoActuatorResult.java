package de.datev.wowlist;

public class TodoActuatorResult {

    private long todoCount;

    public TodoActuatorResult() {
    }

    public TodoActuatorResult(long todoCount) {
        this.todoCount = todoCount;
    }

    public long getTodoCount() {
        return todoCount;
    }

    public void setTodoCount(long todoCount) {
        this.todoCount = todoCount;
    }

}
