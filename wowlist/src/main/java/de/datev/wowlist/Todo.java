package de.datev.wowlist;

import jakarta.persistence.*;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Entity
public class Todo {

    @Id
    private UUID id;
    @Column
    @NotBlank
    @Size(max = 500)
    private String description;

    @Column
    private boolean isDone;
    @Column
    @Future
    private Instant dueDate;

    @OneToMany(cascade = CascadeType.ALL)
    private final List<SubTask> subTasks = new ArrayList<>();

    public Todo(String description, Instant dueDate) {
        this.id = UUID.randomUUID();
        this.description = description;
        this.isDone = false;
        this.dueDate = dueDate;
    }

    protected Todo() {
    }

    public UUID getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }

    public List<SubTask> getSubTasks() {
        return Collections.unmodifiableList(this.subTasks);
    }

    public void addSubTask(SubTask subTask) {
        if (!this.getSubTasks().contains(subTask)) {
            this.subTasks.add(subTask);
        }
        subTask.setTodo(this);
    }
}
