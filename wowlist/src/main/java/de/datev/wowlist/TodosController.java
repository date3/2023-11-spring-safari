package de.datev.wowlist;

import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class TodosController {

    final TodosService todosService;

    public TodosController(TodosService todosService) {
        this.todosService = todosService;
    }

    @GetMapping("/todos")
    public List<TodoDTO> getAllTodos(@Param("description") String description, @Param("dueDate") Instant dueDate) {
        boolean shouldFindByDescription = description != null && !description.isBlank();
        boolean shouldFindByDueDate = dueDate != null;

        List<Todo> todos = new ArrayList<>();

        if (shouldFindByDescription && shouldFindByDueDate) {
            todos = this.todosService.findByDescriptionAndDueDate(description, dueDate);
        } else if (shouldFindByDescription) {
            todos = this.todosService.findByDescription(description);
        } else if (shouldFindByDueDate) {
            todos = this.todosService.findByDueDateBefore(dueDate);
        } else {
            todos = this.todosService.getAllTodos();
        }

        return todos
                .stream()
                .map(TodosController::map)
                .collect(Collectors.toList());
    }

    @GetMapping("/todos/{id}")
    public TodoDTO getTodo(@PathVariable UUID id) {
        return map(this.todosService.getById(id));
    }

    @PostMapping("/todos")
    @ResponseStatus(HttpStatus.CREATED)
    public TodoDTO createTodo(@RequestBody CreateTodoRequest request) {
        return map(this.todosService.createTodo(request.getDescription(), request.getDueDate()));
    }

    @PutMapping("/todos/{id}")
    public TodoDTO updateTodo(@PathVariable UUID id, @RequestBody UpdateTodoRequest request) {
        return map(this.todosService.update(id, request.getDescription(), request.isDone(), request.getDueDate()));
    }

    @DeleteMapping("/todos/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable UUID id) {
        this.todosService.delete(id);
    }

    @PostMapping("/todos/{id}/subtasks")
    @ResponseStatus(HttpStatus.CREATED)
    public void addSubTask(@PathVariable UUID id, @RequestBody AddSubtaskRequest request) {
        this.todosService.addSubTask(id, request.getDescription());
    }

    static TodoDTO map(Todo todo) {
        return new TodoDTO(todo.getId(), todo.getDescription(), todo.isDone(), todo.getDueDate(), todo.getSubTasks());
    }
}
