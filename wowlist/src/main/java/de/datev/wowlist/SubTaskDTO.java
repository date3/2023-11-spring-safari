package de.datev.wowlist;

import java.util.UUID;

public class SubTaskDTO {

    private UUID id;
    private String description;

    public SubTaskDTO(UUID id, String description) {
        this.id = id;
        this.description = description;
    }

    public SubTaskDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
