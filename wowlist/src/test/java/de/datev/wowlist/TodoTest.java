package de.datev.wowlist;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TodoTest {

    @Test
    public void addingASubTaskToATodoSetsTheRelation() {
        Todo todo = new Todo();
        SubTask subTask = new SubTask();

        todo.addSubTask(subTask);

        assertTrue(todo.getSubTasks().contains(subTask));
        assertEquals(todo, subTask.getTodo());
    }

    @Test
    public void subTaskListIsCanNotBeModified() {
        Todo todo = new Todo();

        assertThrows(UnsupportedOperationException.class, () -> {
            todo.getSubTasks().add(new SubTask());
        });
    }

    @Test
    public void addingATodoOnASubTaskSetsTheRelation() {
        Todo todo = new Todo();
        SubTask subTask = new SubTask();

        subTask.setTodo(todo);

        assertTrue(todo.getSubTasks().contains(subTask));
        assertEquals(todo, subTask.getTodo());
    }
}