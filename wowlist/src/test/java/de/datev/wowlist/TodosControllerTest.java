package de.datev.wowlist;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.util.Base64Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.time.Instant;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TodosControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    TodosService todosService;

    final String defaultDescription = "Tee kochen";
    final Instant defaultDueDate = Instant.parse("2023-12-22T10:00:00Z");

    @BeforeEach
    public void setup() {
        this.todosService.deleteAll();
    }

    @Test
    public void dagobertCanGetTodos() throws Exception {
        var request = get("/todos")
                .header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Util.encode("dagobert:duck"));

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void itCanCreateATodoWithADynamicDescription() throws Exception {
        createTodo()
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.description").value(defaultDescription));
    }

    @Test
    public void itCanGetASingleTodo() throws Exception {
        var responseContent = createTodo().andReturn().getResponse().getContentAsString();
        TodoDTO createdTod = objectMapper.readValue(responseContent, TodoDTO.class);

        mockMvc.perform(asDagobert(get("/todos/" + createdTod.getId())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(createdTod.getId().toString()))
                .andExpect(jsonPath("$.description").value(createdTod.getDescription()));
    }

    @Test
    public void itCanUpdateAnExistingTodo() throws Exception {
        String contentAsString = createTodo().andReturn().getResponse().getContentAsString();

        TodoDTO todoDTO = objectMapper.readValue(contentAsString, TodoDTO.class);
        todoDTO.setDescription("Bier brauen");

        mockMvc.perform(asDagobert(put("/todos/" + todoDTO.getId())
                        .content(objectMapper.writeValueAsString(todoDTO))
                        .contentType(MediaType.APPLICATION_JSON)))
                .andReturn().getResponse().getContentAsString();

        mockMvc.perform(asDagobert(get("/todos/" + todoDTO.getId())))
                .andExpect(jsonPath("$.description").value("Bier brauen"));
    }

    @Test
    public void itCanDeleteAnExistingTodo() throws Exception {
        String contentAsString = createTodo().andReturn().getResponse().getContentAsString();
        TodoDTO todoDTO = objectMapper.readValue(contentAsString, TodoDTO.class);

        mockMvc.perform(asDagobert(delete("/todos/" + todoDTO.getId())))
                .andExpect(status().isNoContent());

        mockMvc.perform(asDagobert(get("/todos")))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void itCanFindTodosByDescription() throws Exception {
        createTodo();
        var request = new CreateTodoRequest("Bier brauen", defaultDueDate);
        createTodo(request);

        mockMvc.perform(asDagobert(get("/todos"))).andExpect(jsonPath("$", hasSize(2)));
        mockMvc.perform(asDagobert(get("/todos").param("description", request.getDescription())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].description").value(request.getDescription()));
    }

    @Test
    public void itCanFindTodosByDescriptionAndDueDate() throws Exception {
        createTodo();
        var request = new CreateTodoRequest("Bier brauen", defaultDueDate);
        createTodo(request);
        request.setDueDate(defaultDueDate.minusSeconds(400));
        createTodo(request);

        mockMvc.perform(asDagobert(get("/todos"))).andExpect(jsonPath("$", hasSize(3)));

        mockMvc.perform(asDagobert(get("/todos")
                        .param("description", request.getDescription())
                        .param("dueDate", defaultDueDate.minusSeconds(120L).toString()))
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].description").value(request.getDescription()))
                .andExpect(jsonPath("$[0].dueDate").value(request.getDueDate().toString()));
    }

    @Test
    public void itCanNotCreateWithEmptyDescription() throws Exception {
        var request = new CreateTodoRequest("", defaultDueDate);

        createTodo(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", containsString("Validation failed")))
                .andExpect(jsonPath("$", containsString("description")));
//                .andExpect(jsonPath("$", containsString("must not be blank")));
    }

    @Test
    public void itCanNotCreateWithLongDescription() throws Exception {
        var request = new CreateTodoRequest("A".repeat(600), defaultDueDate);

        createTodo(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", containsString("Validation failed")))
                .andExpect(jsonPath("$", containsString("description")));
//                .andExpect(jsonPath("$", containsString("size must be between 0 and 500")));
    }

    @Test
    public void itCanNotCreateWithDueDateInThePast() throws Exception {
        var request = new CreateTodoRequest("A", Instant.now().minusSeconds(120));

        createTodo(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", containsString("Validation failed")))
                .andExpect(jsonPath("$", containsString("dueDate")));
//                .andExpect(jsonPath("$", containsString("must be a future date")));
    }

    @Test
    public void itCanCreateSubTask() throws Exception {
        var responseContent = createTodo().andReturn().getResponse().getContentAsString();
        TodoDTO createdTodo = objectMapper.readValue(responseContent, TodoDTO.class);

        var addSubtaskRequest = new AddSubtaskRequest("Kaffeee kaufen");

        mockMvc.perform(asDagobert(post("/todos/" + createdTodo.getId() + "/subtasks")
                        .content(objectMapper.writeValueAsString(addSubtaskRequest))
                        .contentType(MediaType.APPLICATION_JSON)))
                .andExpect(status().isCreated());

        mockMvc.perform(asDagobert(get("/todos/" + createdTodo.getId())))
                .andExpect(jsonPath("$.subTasks").isArray())
                .andExpect(jsonPath("$.subTasks", hasSize(1)));
    }

    private ResultActions createTodo(CreateTodoRequest request) throws Exception {
        return mockMvc.perform(asDagobert(post("/todos")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)));
    }

    private ResultActions createTodo() throws Exception {
        var createTodoRequest = new CreateTodoRequest(defaultDescription, defaultDueDate);
        return this.createTodo(createTodoRequest);
    }

    private MockHttpServletRequestBuilder asDagobert(MockHttpServletRequestBuilder requestBuilder) {
        return requestBuilder.header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Util.encode("dagobert:duck"));
    }
}