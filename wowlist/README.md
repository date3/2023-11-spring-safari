# Additional resources

## Day 1

- Builder pattern (e.g. ResponseEntityBuilder, MockMvcRequestBuilder): https://refactoring.guru/design-patterns/builder

## Day 2

- @Value & Spring Expression Language
  - https://www.baeldung.com/spring-value-annotation
  - https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/expressions.html
  - https://www.baeldung.com/spring-expression-language
- Example REST API: https://reqres.in/
- Lombok (@Getter, @Setter): https://www.baeldung.com/intro-to-project-lombok

## Day 3

- Schema management for production applications
  - Liquibase:
    - https://contribute.liquibase.com/extensions-integrations/directory/integration-docs/springboot/springboot/
    - https://www.baeldung.com/liquibase-refactor-schema-of-java-app
  - Flyway:
    - https://documentation.red-gate.com/flyway/quickstart-how-flyway-works/why-database-migrations
    - https://documentation.red-gate.com/fd/community-plugins-and-integrations-spring-boot-184127423.html
    - https://www.baeldung.com/database-migrations-with-flyway